/**
 * RedeemCampaignController
 *
 * @description :: Server-side logic for managing Redeemcampaigns
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	GetOneRedeem: function(req, res) {
		console.log('Get One redeem');
		var params = {};

		for(var k in req.query) {
			params[k] = req.query[k];
		}

		RedeemCampaign.findOne(params)
		.exec(function (err, RedeemCapignResult){
		  if (err) {
		    return res.serverError(err);
		  }
		  if (!RedeemCapignResult) {
		    return res.notFound('Could not find Redeem, sorry.');
		  }

		  sails.log('Found "%s"', RedeemCapignResult);
		  return res.json(RedeemCapignResult);
		});
	},
	UpdateOneRedeem: function(req, res) {
		console.log('Update redeem');	
		console.log(req.body);		
		var params = {};

		for(var k in req.body) {
			params[k] = req.body[k];
		}
		
		console.log(params);
		RedeemCampaign.update({id:req.param('id')}, params).exec(function(err, RedeemCampaignResult) {
			console.log(RedeemCampaignResult);
			return res.json(RedeemCampaignResult);
		});
		
	},
	GetSingleRedeem: function(req, res) {
		console.log('Get Single redeem');
		var params = {};

		for(var k in req.query) {
			params[k] = req.query[k];
		}
		params.id = req.param('id');
		console.log(params);
		RedeemCampaign.find(params).then(function(result, error) {
			if(error) {
				return res.json(401, {err: 'There was a problem please try again later'});
			}
			console.log(result);
			return res.json(result);
		}).catch(function(err) {
			console.log(err)
		});		
	}	
};

	