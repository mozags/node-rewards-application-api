/**
 * UserLoyaltyCardController
 *
 * @description :: Server-side logic for managing Userloyaltycards
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	GetOneLoyaltyCard: function(req, res) {
		console.log('Get One redeem');
		var params = {};

		for(var k in req.query) {
			params[k] = req.query[k];
		}

		UserLoyaltyCard.findOne(params)
		.exec(function (err, RedeemCapignResult){
		  if (err) {
		    return res.serverError(err);
		  }
		  if (!RedeemCapignResult) {
		    return res.notFound('Could not find User loyalty card, sorry.');
		  }

		  sails.log('Found "%s"', RedeemCapignResult);
		  return res.json(RedeemCapignResult);
		});
	},
	UpdateOneLoyaltyCard: function(req, res) {
		
		sails.log.debug('Update loyalty card');

		var params = {};

		for(var k in req.body) {
			params[k] = req.body[k];
		}

		UserLoyaltyCard.findOne({id:req.param('id')})
		.exec(function (err, UserLoyaltyResult){
		  if (err) {
		    return res.serverError(err);
		  }
		  if (!UserLoyaltyResult) {
		    return res.notFound('Could not find User loyalty card, sorry.');
		  }

		  if (typeof UserLoyaltyResult !== 'undefined' && UserLoyaltyResult && typeof UserLoyaltyResult.userId !== 'undefined') {
		    if(UserLoyaltyResult.userId.length > 0) return res.notFound('Loyalty card has alreday been assigned');
		  }
				UserLoyaltyCard.update({id:req.param('id')}, params).exec(function(err, RedeemCampaignResult) {
					if(params.userId) {
						
						/** Start waterfall  */
					        var async = require("async");
					        async.waterfall([
					            function(callback) {

									CampaignUserScans.update({userLoyaltyCardId:req.param('id')},{userId:params.userId})
										.exec(function afterwards(err, updated){

											if (err) {
												return res.serverError(err);
												sails.log.error(new Error(err));
											}
											
											sails.log.debug('User scans updated');
											sails.log.debug(updated);
											sails.log.debug('End User scans updated');
											callback(null, updated);
										});  					            	

					                
					            },
					            function(arg1, callback) {
					                /** START For each **/
					                sails.log.debug('Add campaign');
					               	var campaignsForUser = [];
									async.forEachOf(LoyaltyCardHelper.findUniquecampaignsId(arg1), function(value, key, callback) {
										CampaignUserCampaigns.findOrCreate({userId:params.userId, campaignId:value}, {userId:params.userId, campaignId:value}).exec(function createFindCB(error, createdOrFoundRecords){
										  sails.log.debug('created or found');
										  sails.log.debug(createdOrFoundRecords);
										  sails.log.debug('End created or found');
										  callback();
										});	  	
									}, function(err) {
									    if (err) sails.log.error(new Error(err.message)); 
										Campaign.find({id:LoyaltyCardHelper.findUniquecampaignsId(arg1)}).exec(function createFindCB(error, FoundRecords){
										  callback(null, FoundRecords);
										});	  

									    
									});
					            },
					            function(campaignsList, callback) {
					                /** START For each **/
					                sails.log.debug('Add campaign');
					               	sails.log.debug(campaignsList);
					               	sails.log.debug('End campaign list');
									async.forEachOf(campaignsList, function(value, key, callback) {
										sails.log.debug(value);
										CampaignUserScans.find({userId:params.userId, campaignId:value.id, active: 1})
										.exec(function createFindCB(error, scansResults){
										  sails.log.debug('Scans Found');

										  var campainsScans = [], size = value.visitsBeforeDiscount;
										  while (scansResults.length > 0) {
    											campainsScans.push(scansResults.splice(0, size));
    											var campScans = campainsScans[campainsScans.length-1];
    											
    											if(campScans.length == value.visitsBeforeDiscount)
    											{
    												// create a redeem
    												RedeemCampaign.create({userId:params.userId, campaignId:value.id, scanObjectCollections:campScans, active:1, archive:0})
															.exec(function (error, redeemCreated){
		    													if(redeemCreated) {
		    														// we update the scans 
		    														sails.log.debug('Redeem created');
		    														sails.log.debug(redeemCreated);
		    														sails.log.debug(LoyaltyCardHelper.findUniqueIds(campScans, 'id'));
		    														sails.log.debug('End Redeem created');

																	CampaignUserScans.update({id:LoyaltyCardHelper.findUniqueIds(campScans, 'id')},{active:0, archive:1})
																		.exec(function afterwards(err, updatedScans){
																			sails.log.debug('User scans updated after redeem');
																			sails.log.debug(updatedScans);
																			sails.log.debug('End User scans updated after redeem');
																		});		    														
		    													}
															});
    											}

    											sails.log.debug(campScans.length +"=="+ value.visitsBeforeDiscount);

											}

										  sails.log.debug(campainsScans);
										  
										  sails.log.debug('End Scans Found');
										  callback();
										});	  	
									}, function(err) {
									    if (err) console.error(err.message); 
									    sails.log.debug('Updated scans');
									    sails.log.debug(campaignsList);
									    sails.log.debug('end Updated scans');
									    callback();
									});
					            }					            
					        ], function(err, result) {
					            // result now equals 'done'
					            return res.json(RedeemCampaignResult);
					        });
        /** End waterfall */

					}
				});		  
		});
		
	},

	GetSingleLoyaltyCard: function(req, res) {
		console.log('Get Single Loyalty card');
		var params = {};

		for(var k in req.query) {
			params[k] = req.query[k];
		}
		params.id = req.param('id');
		console.log(params);

		UserLoyaltyCard.findOne(params)
		.exec(function (err, UserLoyaltyCardResult){
		  if (err) {
		    return res.serverError(err);
		  }
		  if (!UserLoyaltyCardResult) {
		    return res.notFound('Could not find user loyalty card, sorry.');
		  }
		  console.log(UserLoyaltyCardResult);
		  sails.log('Found "%s"', UserLoyaltyCardResult);
		  return res.json(UserLoyaltyCardResult);
		});

		// UserLoyaltyCard.findOne(params).then(function(result, error) {
		// 	if(error) {
		// 		return res.json(401, {err: 'There was a problem please try again later'});
		// 	}
		// 	console.log(result);
		// 	return res.json(result);
		// }).catch(function(err) {
		// 	console.log(err)
		// });		
	},
	CreateBulkLoyaltyCards: function(req, res) {

		var loyaltyCardsArray = [];

		for(var i=0; i < req.param('loyalty_cards_to_create'); i++) {
			loyaltyCardsArray[i] = {};
			loyaltyCardsArray[i]['campaignOwnerUserId'] = req.token.id;
			loyaltyCardsArray[i]['active'] = 1;
			loyaltyCardsArray[i]['archive'] = 0;
			loyaltyCardsArray[i]['userId'] = '';
			
		}

		console.log(loyaltyCardsArray);

		UserLoyaltyCard.create(loyaltyCardsArray).exec(function (err, loyaltyCards){
		  if (err) { return res.serverError(err); }

		  sails.log('Finn\'s id is:', loyaltyCards);
		  return res.json(loyaltyCards);
		});
	}
};

