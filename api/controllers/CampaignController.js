/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	GetCampaign: function(req, res) {

        if (!req.token.id) {
            return res.serverError(err);
        }

	    var campaignOwnerUserId = req.token.id; //req.param('campaignOwnerUserId');
    	Campaign.find({campaignOwnerUserId:campaignOwnerUserId}, function(err, campaign) {
    		if(!err) {
    			res.json(campaign);
    		}
    	});

	}
};

