/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	GetUsers: function(req, res) {
	    CampaignUser.find({select:['email', 'username']},function(err, users) {
	        return res.json(users);
	    });
	},
	GetUserById: function(req, res) {
		CampaignUser.findOne({id:req.param('user_id')})
		.exec(function (err, user){
		  if (err) {
		    return res.serverError(err);
		  }
		  if (!user) {
		    return res.notFound('Could not find user');
		  }

		  sails.log('Found User "%s"', user);
		  return res.json(user);
		});
	},	
	UpdateUser: function(req, res) {
		console.log('Update user');	
		console.log(req.body);		
		var params = {};

		for(var k in req.body) {
			params[k] = req.body[k];
		}
		
		console.log(params);
		CampaignUser.update({id:req.param('user_id')}, params).exec(function(err, campaignUserResult) {
			console.log(campaignUserResult);
			return res.json(campaignUserResult[0]);
		});
		
	},	
	RegisterUser: function(req, res) {
		console.log('Register user');	
		console.log(req.body);		
		var params = {};

		for(var k in req.body) {
			params[k] = req.body[k];
		}
		
		console.log(params);
		CampaignUser.create(params).exec(function(err, campaignUserResult) {
			console.log(campaignUserResult);
			return res.json(campaignUserResult);
		});
		
	},	

};

