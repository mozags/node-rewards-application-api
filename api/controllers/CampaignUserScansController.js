/**
 * CampaignUserScansController
 *
 * @description :: Server-side logic for managing Campaignuserscans
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	AddUserScan: function(req, res) {

        console.log(req.body);
		// var userId = req.param('userId');
  //   	var campaignId = req.param('campaignId');
    	
    	// Create object
    	// var userScan = {};
    	// userScan.userId = userId;
    	// userScan.campaignId = campaignId;
    	// userScan.active = 1;
    	// userScan.archive = 0;

        // validate required 
        if(!req.param('userId') && !req.param('userLoyaltyCardId')) {
            return res.send(400, 'You need to provide userId or userLoyaltyCardId');
        }
        
        if(!req.param('campaignId')) {
            return res.send(400, 'You need to provide campaignId');
        }        
        var params = {};

        for(var k in req.body) {
            params[k] = req.body[k];
        }

        var async = require("async");
        async.waterfall([
            function(callback) {
               if(params['userLoyaltyCardId']) {
                    UserLoyaltyCard.findOne({id:params['userLoyaltyCardId']})
                        .exec(function (err, UserLoyaltyCardResult){
                           if(err) return res.send(400, err);
                           if(typeof(UserLoyaltyCardResult.userId) != "undefined" && UserLoyaltyCardResult.userId.length > 0) {
                                params['userId'] = UserLoyaltyCardResult.userId;
                           }

                           callback();
                        });
                }
                if(req.param('userId')) {
                    callback();
                }

            },
            function(callback) {
                //Add scan object
                CampaignUserScans.create(params).exec(function(err, userScan) {
                    if (err) { return res.serverError(err); }
                    callback(null, userScan);
                });                
                /* End foreach */
            }
        ], function(err, result) {
            // result now equals 'done'
            return res.json(result);
        });        

	}
};

