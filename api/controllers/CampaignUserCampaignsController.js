/**
 * CampaignUserCampaignsController
 *
 * @description :: Server-side logic for managing Campaignusercampaigns
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    GetUserCampaigns: function(req, res) {

        if (!req.token.id) {
            return res.serverError(err);
        }
        var async = require("async");
        async.waterfall([
            function(callback) {
                var userId = req.token.id;
                CampaignUserCampaigns.find({ userId: userId }, function(err, campaigns) {
                    callback(null, campaigns);
                });
            },
            function(arg1, callback) {

                /** START For each **/
                var campaignNew = [];

                async.forEachOf(arg1, function(value, key, callback) {
                    //console.log(value);
                    campaignNew[key] = value;
                    Campaign.findOne({ id: value.campaignId }, function(err, campaign) {
                        campaignNew[key].campaign = campaign;
                        callback();
                    });
                }, function(err) {
                    if (err) console.error(err.message);
                    
                    callback(null, campaignNew);
                    
                    
                });
                /* End foreach */


            },
            function(arg1, callback) {
                console.log(arg1);

                var campaignNew = [];

                async.forEachOf(arg1, function(value, key, callback) {
                    campaignNew[key] = value;
                    CampaignUserScans.count({ userId: value.userId, campaignId: value.campaignId, active: 1 })
                        .exec(function countCB(error, found) {
                            campaignNew[key]['campaign'].uservisits = found
                            callback();
                        });
                }, function(err) {
                    if (err) console.error(err.message);
                    callback(null, campaignNew);
                });
            },
            function(arg1, callback) {
                var campaignNew = [];
                async.forEachOf(arg1, function(value, key, callback) {
                    campaignNew[key] = value;
                    RedeemCampaign.find({ userId: value.userId, campaignId: value.campaignId, active: 1 })
                        .exec(function countCB(error, result) {
                            campaignNew[key]['campaign'].redeem = result
                            callback();
                        });
                }, function(err) {
                    if (err) console.error(err.message);
                    callback(null, campaignNew);
                });
            }
        ], function(err, result) {
            // result now equals 'done'
            return res.json(result);
        });
    }
};
