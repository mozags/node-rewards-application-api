/**
 * CampaignOwnerController
 *
 * @description :: Server-side logic for managing Campaignowners
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	GetCampaignOwner: function(req, res) {
		console.log('nooooo');
		return res.json('nooooo');
	},
	RegisterCampaignOwner: function(req, res) {
		console.log('Register new campaign owner');
		
		var params = {};

		for(var k in req.body) {
			params[k] = req.body[k];
		}
		
		console.log(params);

		CampaignOwnerUser.create(params).exec(function(err, campaignOwnerUserResult) {
			console.log(campaignOwnerUserResult);
			return res.json(campaignOwnerUserResult);
		});		
	
	},
};

