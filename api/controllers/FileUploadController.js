
module.exports = {
	uploadCamapignFile: function(req, res) {
		req.file('file').upload({
  					dirname: '../../assets/campaign-images'},
  			function (err, uploadedFiles) {
	 			console.log(uploadedFiles[0]);

	 			if(uploadedFiles == 0) {
	 				return res.send(400, 'File could not be uploaded');
	 			}
	 			var getFilename = uploadedFiles[0].fd.split('/');
	 			Campaign.update({id:req.param('campaign_id')}, {campaignImage: getFilename[getFilename.length-1]})
	 				.exec(function (err, updated){
				      	if (err) return res.negotiate(err);
				      	return res.json(updated);
				    });
	 				/**
	 				 * Resize image to maintanable size
	 				 */
					var lwip = require('lwip');
					console.log('assets/campaign-images/'+getFilename[getFilename.length-1]);
					// obtain an image object:
					lwip.open('assets/campaign-images/'+getFilename[getFilename.length-1], function(err, image){
						image.resize(500, 330, 'moving-average', function(err, image) {
							image.writeFile('assets/campaign-images-resized/'+getFilename[getFilename.length-1], function(file) {
								console.log(file);
								});
						});
					});
			});

	},
	uploadUsersProfileFile: function(req, res) {
		req.file('file').upload({
  					dirname: '../../assets/users-profile-images'},
  			function (err, uploadedFiles) {
	 			console.log(uploadedFiles[0]);

	 			if(uploadedFiles == 0) {
	 				return res.send(400, 'File could not be uploaded');
	 			}
	 			var getFilename = uploadedFiles[0].fd.split('/');
	 			CampaignUser.update({id:req.param('user_id')}, {profilePicture: getFilename[getFilename.length-1]})
	 				.exec(function (err, updated){
				      	if (err) return res.negotiate(err);
				      	return res.json(updated);
				    });
	 				/**
	 				 * Resize image to maintanable size
	 				 */
					var lwip = require('lwip');
					console.log('assets/users-profile-images/'+getFilename[getFilename.length-1]);
					// obtain an image object:
					lwip.open('assets/users-profile-images/'+getFilename[getFilename.length-1], function(err, image){
						image.resize(500, 330, 'moving-average', function(err, image) {
							image.writeFile('assets/users-profile-images-resized/'+getFilename[getFilename.length-1], function(file) {
								console.log(file);
								});
						});
					});				    
			});

	}	

};