/**
 * CampaignUserCampaigns.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	tableName: 'campaign-user-campaigns',
	attributes: {
		campaignId: {
			type: 'string'
		},
		userId: {
			type: 'string'
		},
		dateTimeCreated: {
			type: 'string'
		},
		active: {
			type: 'interger'
		},
		archive: {
			type: 'interger'
		},
		campaignOwnerId: {
			type: 'string'
		},
		campaignOwnerUserId: {
			type: 'string'
		}
	}
};

