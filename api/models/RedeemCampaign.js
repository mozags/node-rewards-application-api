/**
 * RedeemCampaign.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'redeem-campaign',
  attributes: {
  	campaignId: {
  		type: 'string'
  	},
  	userId: {
  		type: 'string'
  	},
  	campaignOwnerUserId: {
  		type: 'string'
  	},
   	active: {
  		type: 'integer'
  	},
   	archive: {
  		type: 'integer'
  	},
  	scanObjectCollections: {
  		type: 'array'
  	}
  }
};

