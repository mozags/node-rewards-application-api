/**
 * UserLoyaltyCard.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'user-loyalty-card',
  attributes: {
  	userId: {
  		type: 'string'
  	},
  	campaignOwnerUserId: {
  		type: 'string'
  	},
  	active: {
  		type: 'integer'
  	},
   	archive: {
  		type: 'integer'
  	}, 	
  }
};

