/**
 * CampaignOwner.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	tableName: 'campaign-owner-user',
	attributes: {
		username: {
			 type: 'string',
			 unique: true
		},
		email: {
			type: 'email',
			unique: true
		},
		companyName: {
			type: 'string'
		},
		address: {
			type: 'string'
		},
		postCode: {
			type: 'string'
		},
		telephonNo: {
			type: 'string'
		},
		mobileNo: {
			type: 'string'
		},
		campaignOwnerId: {
			type: 'string'
		},
		fullName: {
			type: 'string'
		},
		password: {
			type: 'string'
		},
	    // We don't wan't to send back encrypted password either
	    toJSON: function () {
	      var obj = this.toObject();
	      delete obj.password;
	      return obj;
	    }		
	},
 // Here we encrypt password before creating a User
  beforeCreate : function (values, next) {
    sails.bcrypt.genSalt(10, function (err, salt) {
      if(err) return next(err);
      sails.bcrypt.hash(values.password, salt, function (err, hash) {
        if(err) return next(err);
        values.password = hash;
        next();
      })
    })
  },

  comparePassword : function (password, user, cb) {
    sails.bcrypt.compare(password, user.password, function (err, match) {

      if(err) cb(err);
      if(match) {
        cb(null, true);
      } else {
        cb(err);
      }
    })
  }	
};

