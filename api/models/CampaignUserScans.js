/**
 * CampaignUserScans.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: 'campaign-user-scans',
  attributes: {
  	userId: {
  		type: 'string'
  	},
  	campaignId: {
  		type: 'string',
  		required: true
  	},
  	userLoyaltyCardId: {
  		type:'string'
  	},
  	active: {
  		type: 'integer'
  	},
  	archive: {
  		type: 'integer'
  	}  	
  },
  /**
   * After the record is created we need 
   * to update User scans
   * @param  {object} values new object created
   * @param  {Function} next   callback function
   */
  afterCreate : function (values, next) {
    if(values.userId && values.userId.length > 0) {
      this.addCampaignToUser(values);
      this.getCampaign(values, this.upDateUserRedeem, next);
    }
    return next();
  },

  getCampaign: function(scanvalues, callback, next) {
    Campaign.findOne({id:scanvalues.campaignId}, function(err, campaign) {
      if(err) { return next(err) }
      // console.log(campaign);
      return callback(scanvalues, campaign, next);
    });
  },
  // Update user scans for redeeming 
  upDateUserRedeem: function(scanValues, campaign, next) {
    this.find({userId:scanValues.userId, campaignId:scanValues.campaignId, active:1},function(err, results) {
      if(results.length >= campaign.visitsBeforeDiscount) {
        
        var myQuery = CampaignUserScans.find({userId:scanValues.userId, campaignId:scanValues.campaignId, active:1});
            myQuery.limit(campaign.visitsBeforeDiscount);
            myQuery.exec(function callBack(err,limitedResults){
                // console.log(limitedResults);
                RedeemCampaign.create({
                  campaignId: scanValues.campaignId, 
                  userId: scanValues.userId, 
                  scanObjectCollections: limitedResults, 
                  campaignOwnerUserId: campaign.campaignOwnerUserId, 
                  campaignOwnerId: campaign.campaignOwnerId, 
                  active: 1, 
                  archive: 0
                }).exec(function(err, redeemObject) {
                  if(!err) {
                      for(var i=0; i < limitedResults.length; i++){
                        CampaignUserScans.update(limitedResults[i].id, {active:0, archive:1}).exec(function(err, campainScansUpdatedObject) {
                          console.log('Updated scan');
                          console.log(campainScansUpdatedObject);
                          console.log('End Updated scan');
                        });
                    }
                  }
                });
               
            });
          
      } else {
        
      } 
      
    });
  },
  // add the campaign to the user if it does not exist
  addCampaignToUser: function(scanValues, next) {
    CampaignUserCampaigns.find({userId:scanValues.userId, campaignId:scanValues.campaignId})
      .exec(function(error, found) {
        console.log('Test if exists campaign');
        console.log(found);
        if(found.length == 0) {
          console.log('Add new cmapaign');
          CampaignUserCampaigns.create({userId:scanValues.userId, campaignId:scanValues.campaignId})
            .exec(function(err, userCampaignObject) {
              // console.log(userCampaignObject);
              //return next();
            });
        }
      });
  }  
};

