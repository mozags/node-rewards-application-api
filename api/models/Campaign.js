/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	tableName: 'campaign',
	attributes: {
		title: {
			 type: 'string'
		},
		description: {
			 type: 'string'
		},	
		redeemTitle: {
			 type: 'string'
		},
		redeemDescription: {
			 type: 'string'
		},			
		visitsBeforeDiscount: {
			 type: 'integer'
		},
		campaignOwnerUserId: {
			 type: 'string'
		},	
		dateCampaignStarts: {
			 type: 'string'
		},	
		dateCampaignEnds: {
			 type: 'string'
		},	
		active: {
			 type: 'integer'
		},											
		archive: {
			 type: 'integer'
		},	
		companyName: {
			 type: 'string'
		},	
		dateTimeCreated: {
			 type: 'string'
		},	
		campaignImage: {
			 type: 'string'
		},	
		dateTimeUpdated: {
			 type: 'string'
		}										
	}
};

