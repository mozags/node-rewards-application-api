/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
	tableName: 'campaign-user',
	attributes: {
		mobileNo: {
			 type: 'string'
		},
		email: {
			type: 'email',
			unique: true
		},
		dateTimeCreated: {
			type: 'string'
		},
		dateTimeUpdated: {
			"type": "string"
		},
		firstName: {
			"type": "string"	
		},
		lastName: {
			"type": "string"	
		},	
		gender: {
			"type": "string"	
		},
		dob: {
			"type": "string"	
		},
		allowClientsToViewMyProfile: {
			"type": "string"	
		},						
		acceptTandCs: {
			"type": "string"	
		},
		password: {
			type: 'string'
		},
		profilePicture: {
			type: 'string'
		},		
	    // We don't wan't to send back encrypted password either
	    toJSON: function () {
	      var obj = this.toObject();
	      delete obj.password;
	      return obj;
	    }			
	},
 // Here we encrypt password before creating a User
  beforeCreate : function (values, next) {
    sails.bcrypt.genSalt(10, function (err, salt) {
      if(err) return next(err);
      sails.bcrypt.hash(values.password, salt, function (err, hash) {
        if(err) return next(err);
        values.password = hash;
        next();
      })
    })
  },

  comparePassword : function (password, user, cb) {
    sails.bcrypt.compare(password, user.password, function (err, match) {

      if(err) cb(err);
      if(match) {
        cb(null, true);
      } else {
        cb(err);
      }
    })
  },
  beforeUpdate: function (attrs, cb) {
    if(attrs.newPassword){
      sails.bcrypt.genSalt(10, function(err, salt) {
        if (err) return cb(err);

        sails.bcrypt.hash(attrs.newPassword, salt, function(err, crypted) {
          if(err) return cb(err);

          delete attrs.newPassword;
          attrs.password = crypted;
          return cb();
        });
      });
    }
    else {
      return cb();
    }
  }  		
};

